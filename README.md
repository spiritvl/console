# Console

Command line interface.

## Installation

The preferred method of installation is via [Composer](http://getcomposer.org/). Run the following
command to install the package and add it as a requirement to your project's
`composer.json`:

```bash
composer require spiritvl/console
```

## Quality

### Unit tests

```bash
php vendor/bin/phpunit
```

### phpStan
```bash
vendor/bin/phpstan analyse src
```

### psalm
```bash
vendor/bin/psalm
```

## Copyright and License

The spiritvl/console library is copyright © [Moiseev Dmitry](mailto:spiritvl@gmail.com)
and licensed for use under the MIT License (MIT).
