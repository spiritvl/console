<?php

namespace Spiritvl\Console\Command;

use RuntimeException;
use Spiritvl\Console\CommandInterface;
use Spiritvl\Console\Input\Parameters\ParametersBag;
use Spiritvl\Console\Output\StringBuffer;

class CommandList
{
    /**
     * 
     *
     * @var array<CommandInterface> 
     */
    private array $commands = [];

    /**
     * @param  array<CommandInterface> $commands
     * @return void
     */
    public function registerMany(array $commands): void
    {
        foreach($commands as $command) {
            $this->register($command);
        }
    }

    public function register(CommandInterface $command): void
    {
        $this->commands[$command->name()] = $command;
    }

    public function run(string $name, ParametersBag $parameters): string
    {
        return $this->get($name)->run($parameters);
    }

    public function help(string $name): string
    {
        return $this->get($name)->description();
    }

    public function list(): string
    {
        $buffer = new StringBuffer();

        foreach ($this->commands as $command) {
            $buffer->addLine($command->name());
        }

        return $buffer->toString();
    }

    private function get(string $name): CommandInterface
    {
        if (isset($this->commands[$name])) {
            return $this->commands[$name];
        }

        throw new RuntimeException('Command "' . $name . '" not found!');
    }
}
