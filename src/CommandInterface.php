<?php

namespace Spiritvl\Console;

use Spiritvl\Console\Input\Parameters\ParametersBag;

interface CommandInterface
{
    public function name(): string;

    public function description(): string;

    public function run(ParametersBag $parameters): string;
}
