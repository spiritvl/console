<?php
namespace Spiritvl\Console;

use RuntimeException;
use Spiritvl\Console\Command\CommandList;
use Spiritvl\Console\Input\InputParser;
use Spiritvl\Console\Output\StringBuffer;

class Console
{
    private CommandList $commands;
    private InputParser $parser;

    /**
     * @param array<CommandInterface> $commands
     */
    public function __construct(array $commands)
    {
        $this->parser = new InputParser();

        $this->commands = new CommandList();
        $this->commands->registerMany($commands);
    }

    public function registerCommand(CommandInterface $command): void
    {
        $this->commands->register($command);
    }

    /**
     * @param  string[] $argv
     * @return string
     * @throws RuntimeException
     */
    public function handleInput(array $argv): string
    {
        $buffer = new StringBuffer();

        $this->parser->parse($argv);

        if ($this->parser->isEmptyCommand()) {
            $buffer->addLine('Available commands:');
            $buffer->addLine($this->commands->list());
            return $buffer->toString();
        }

        if ($this->parser->isHelp()) {
            $buffer->addLine($this->parser->getCommand());
            $buffer->addLine($this->commands->help($this->parser->getCommand()));
            return $buffer->toString();
        }

        $result = $this->commands->run($this->parser->getCommand(), $this->parser->getParameters());
        $buffer->addLine($result);
        return $buffer->toString();
    }
}
