<?php

namespace Spiritvl\Console\Input;

use Spiritvl\Console\Input\Parameters\ParametersBag;

class InputParser
{
    private string $fileName = '';
    private string $command = '';
    private ParametersBag $parameters;
    private ParametersParser $parser;

    public function __construct()
    {
        $this->parameters = new ParametersBag();
        $this->parser = new ParametersParser();
    }

    /**
     * @param  string[] $argv
     * @return void
     */
    public function parse(array $argv): void
    {
        // First parameter always filename
        $this->fileName = $argv[0];

        if (count($argv) >= 2) {
            // Second parameter is command name, optional
            $this->command = $argv[1];
        }

        if (count($argv) > 2) {
            // Other parameters is options and arguments
            $this->parameters = $this->parser->parse(array_slice($argv, 2));
        }
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getCommand(): string
    {
        return $this->command;
    }

    public function getParameters(): ParametersBag
    {
        return $this->parameters;
    }

    public function isEmptyCommand(): bool
    {
        return empty($this->command);
    }

    public function isHelp(): bool
    {
        return in_array('help', $this->parameters->arguments()->all(), true);
    }
}
