<?php

namespace Spiritvl\Console\Input\Parameters;

class Arguments
{
    /**
     * 
     *
     * @var string[] 
     */
    private array $arguments = [];

    /**
     * @param  string[] $arguments
     * @return $this
     */
    public function add(array $arguments): self
    {
        $this->arguments = array_unique(array_merge($this->arguments, $arguments));
        return $this;
    }

    /**
     * @return string[]
     */
    public function all(): array
    {
        return $this->arguments;
    }

    public function isExist(string $argument): bool
    {
        return in_array($argument, $this->arguments, true);
    }
}
