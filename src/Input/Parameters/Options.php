<?php

namespace Spiritvl\Console\Input\Parameters;

use RuntimeException;

class Options
{
    /**
     * 
     *
     * @var array<string, string|string[]> 
     */
    private array $options = [];

    /**
     * @param  array<string, string|string[]> $options
     * @return $this
     */
    public function add(array $options): self
    {
        foreach ($options as $key => $value) {
            $this->options[$key] = $value;
        }
        return $this;
    }

    /**
     * @return array<string, string|string[]>
     */
    public function all(): array
    {
        return $this->options;
    }

    public function isExist(string $option): bool
    {
        return isset($this->options[$option]);
    }

    /**
     * @param  string $option
     * @return string|string[]
     */
    public function get(string $option): string|array
    {
        if ($this->isExist($option)) {
            return $this->options[$option];
        }

        throw new RuntimeException('Option [' . $option . '] does not exist!');
    }
}
