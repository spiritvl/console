<?php

namespace Spiritvl\Console\Input\Parameters;

class ParametersBag
{
    private Arguments $arguments;
    private Options $options;

    public function __construct()
    {
        $this->arguments = new Arguments();
        $this->options = new Options();
    }

    public function arguments(): Arguments
    {
        return $this->arguments;
    }

    public function options(): Options
    {
        return $this->options;
    }
}
