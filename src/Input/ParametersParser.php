<?php

namespace Spiritvl\Console\Input;

use Spiritvl\Console\Input\Parameters\ParametersBag;
use Spiritvl\Console\Input\Parsers\ArgumentsParser;
use Spiritvl\Console\Input\Parsers\OptionsParser;

class ParametersParser
{
    private ArgumentsParser $argumentsParser;
    private OptionsParser $optionsParser;

    public function __construct()
    {
        $this->argumentsParser = new ArgumentsParser();
        $this->optionsParser = new OptionsParser();
    }

    /**
     * @param  string[] $parameters
     * @return ParametersBag
     */
    public function parse(array $parameters): ParametersBag
    {
        $bag = new ParametersBag();

        foreach ($parameters as $parameter) {
            if ($this->argumentsParser->isArgument($parameter)) {
                $bag->arguments()->add($this->argumentsParser->parse($parameter));
                continue;
            }

            if ($this->optionsParser->isOption($parameter)) {
                $bag->options()->add($this->optionsParser->parse($parameter));
                continue;
            }
        }

        return $bag;
    }
}
