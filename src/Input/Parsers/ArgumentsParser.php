<?php

namespace Spiritvl\Console\Input\Parsers;

use RuntimeException;

class ArgumentsParser
{
    public function isArgument(string $value): bool
    {
        // Аргументы имеют формат {name} или {arg,name}
        return (bool)preg_match('/^{.*}$/', $value);
    }

    /**
     * @param  string $value
     * @return string[]
     */
    public function parse(string $value): array
    {
        if (!$this->isArgument($value)) {
            throw new RuntimeException('This is not an argument! ' . $value);
        }

        $value = str_replace(['{', '}'], '', $value);
        if (str_contains($value, ',')) {
            return explode(',', $value);
        }

        return [$value];
    }
}
