<?php

namespace Spiritvl\Console\Input\Parsers;

use \RuntimeException;

class OptionsParser
{
    public function isOption(string $value): bool
    {
        // Аргументы имеют формат [name=val] или [name=val,other]
        return (bool)preg_match('/^\[.*]$/', $value);
    }

    /**
     * @param  string $value
     * @return array<string, string|string[]>
     */
    public function parse(string $value): array
    {
        if (!$this->isOption($value)) {
            throw new RuntimeException('This is not an option! ' . $value);
        }

        $value = str_replace(['[', ']'], '', $value);
        $parts = explode('=', $value);

        // Левая часть это ключ
        $name = $parts[0];

        // Правая часть это или значение или массив значений
        if (str_contains($parts[1], ',')) {
            return [
                $name => explode(',', $parts[1])
            ];
        }

        return [
            $name => $parts[1]
        ];
    }
}
