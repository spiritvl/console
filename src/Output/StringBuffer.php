<?php

namespace Spiritvl\Console\Output;

class StringBuffer
{
    /**
     * 
     *
     * @var string[] 
     */
    private array $lines;

    /**
     * @param string[] $lines
     */
    public function __construct(array $lines = [])
    {
        $this->lines = $lines;
    }

    public function addLine(string $line): self
    {
        $this->lines[] = $line;
        return $this;
    }

    /**
     * @param  string[] $lines
     * @return $this
     */
    public function addLines(array $lines): self
    {
        $this->lines = array_merge($this->lines, $lines);
        return $this;
    }

    /**
     * @return string[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    public function clear(): self
    {
        $this->lines = [];
        return $this;
    }

    public function toString(): string
    {
        if (empty($this->getLines())) {
            return '';
        }

        return implode(PHP_EOL, $this->getLines()) . PHP_EOL;
    }
}
