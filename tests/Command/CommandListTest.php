<?php

namespace Spiritvl\Console\Tests\Command;

use PHPUnit\Framework\TestCase;
use RuntimeException;
use Spiritvl\Console\Command\CommandList;
use Spiritvl\Console\CommandInterface;
use Spiritvl\Console\Input\Parameters\ParametersBag;
use Spiritvl\Console\Tests\Helpers\CommandFactory;

class CommandListTest extends TestCase
{
    private CommandFactory $factory;

    public function __construct()
    {
        $this->factory = new CommandFactory($this->getMockBuilder(CommandInterface::class));
        parent::__construct();
    }

    public function testRegister(): void
    {
        $list = new CommandList();
        $first = $this->factory->createCommand('first', 'first description', 'first result');
        $second = $this->factory->createCommand('second', 'second description', 'second result');

        $list->registerMany([$first, $second]);

        $this->assertEquals('first description', $list->help('first'));
    }

    public function testErrorName(): void
    {
        $list = new CommandList();

        $this->expectException(RuntimeException::class);
        $list->help('not-exist');
    }

    public function testList(): void
    {
        $list = new CommandList();
        $first = $this->factory->createCommand('c1', 'desc1', '');
        $second = $this->factory->createCommand('c2', 'desc2', '');

        $list->registerMany([$first, $second]);

        $this->assertEquals("c1\nc2\n", $list->list());
    }

    public function testRun(): void
    {
        $list = new CommandList();
        $command = $this->factory->createCommand('command', 'description', 'result');

        $list->register($command);

        $this->assertEquals('result', $list->run('command', new ParametersBag()));
    }
}
