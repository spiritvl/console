<?php

namespace Spiritvl\Console\Tests\Helpers;

use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use Spiritvl\Console\CommandInterface;

class CommandFactory
{
    private MockBuilder $builder;

    public function __construct(MockBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function createCommand(string $name, string $description, string $result): MockObject|CommandInterface
    {
        $command = $this->builder->getMock();

        $command->method('name')->willReturn($name);
        $command->method('description')->willReturn($description);
        $command->method('run')->willReturn($result);

        return $command;
    }
}
