<?php

namespace Spiritvl\Console\Tests\Helpers;

use Spiritvl\Console\Input\Parameters\ParametersBag;

class ParametersBagFactory
{
    private ParametersBag $bag;

    public function __construct()
    {
        $this->bag = new ParametersBag();
    }

    public function withArguments(array $arguments): self
    {
        $this->bag->arguments()->add($arguments);
        return $this;
    }

    public function withOptions(array $options): self
    {
        $this->bag->options()->add($options);
        return $this;
    }

    public function create(): ParametersBag
    {
        $bag = clone $this->bag;
        $this->bag = new ParametersBag();

        return $bag;
    }
}
