<?php

namespace Spiritvl\Console\Tests\Input;

use \RuntimeException;
use PHPUnit\Framework\TestCase;
use Spiritvl\Console\CommandInterface;
use Spiritvl\Console\Console;
use Spiritvl\Console\Tests\Helpers\CommandFactory;

class ConsoleTest extends TestCase
{
    private CommandFactory $factory;

    public function __construct()
    {
        $this->factory = new CommandFactory($this->getMockBuilder(CommandInterface::class));
        parent::__construct();
    }

    public function testEmptyList(): void
    {
        $console = new Console([]);

        $result = $console->handleInput(['app.php']);

        $this->assertEquals("Available commands:\n\n", $result);
    }

    public function testList(): void
    {
        $console = new Console([
            $this->factory->createCommand('first', 'first description', 'first result'),
            $this->factory->createCommand('second', 'second description', 'second result'),
        ]);

        $result = $console->handleInput(['app.php']);

        $this->assertEquals("Available commands:\nfirst\nsecond\n\n", $result);
    }

    public function testRun(): void
    {
        $console = new Console([
            $this->factory->createCommand('first', 'first description', 'first result'),
        ]);

        $result = $console->handleInput(['app.php', 'first']);

        $this->assertEquals("first result\n", $result);
    }

    public function testErrorRun(): void
    {
        $console = new Console([
            $this->factory->createCommand('first', 'first description', 'first result'),
        ]);

        $this->expectException(RuntimeException::class);
        $console->handleInput(['app.php', 'second']);
    }
}
