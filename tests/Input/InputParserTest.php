<?php

namespace Spiritvl\Console\Tests\Input;

use Spiritvl\Console\Input\InputParser;
use Spiritvl\Console\Input\Parameters\ParametersBag;
use Spiritvl\Console\Tests\Helpers\ParametersBagFactory;
use PHPUnit\Framework\TestCase;

class InputParserTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testParse(array $argv, string $command, ParametersBag $parameters, bool $isEmptyCommand): void
    {
        $parser = new InputParser();

        $parser->parse($argv);

        $this->assertEquals($command, $parser->getCommand());
        $this->assertEquals($parameters, $parser->getParameters());
        $this->assertEquals($isEmptyCommand, $parser->isEmptyCommand());
    }

    public function testHelp(): void
    {
        $parser = new InputParser();

        $this->assertFalse($parser->isHelp());

        $parser->parse(['app.php', 'command', '{help}']);
        $this->assertTrue($parser->isHelp());
    }

    public function dataProvider(): array
    {
        return [
            [
                ['app.php'],
                '',
                (new ParametersBagFactory())->create(),
                true
            ],
            [
                ['app.php', 'command'],
                'command',
                (new ParametersBagFactory())->create(),
                false
            ],
            [
                ['app.php', 'command', '{arg}'],
                'command',
                (new ParametersBagFactory())->withArguments(['arg'])->create(),
                false
            ],
            [
                ['app.php', 'command', '[name=value]'],
                'command',
                (new ParametersBagFactory())->withOptions(['name' => 'value'])->create(),
                false
            ],
            [
                ['app.php', 'command', '{arg}', '[name=value]'],
                'command',
                (new ParametersBagFactory())->withArguments(['arg'])->withOptions(['name' => 'value'])->create(),
                false
            ],
            [
                ['app.php', 'command', '{arg1,arg2}', '[name=value]', '{arg3}'],
                'command',
                (new ParametersBagFactory())->withArguments(['arg1', 'arg2', 'arg3'])->withOptions(['name' => 'value'])->create(),
                false
            ],
            [
                ['app.php', 'command', '[name=value]', '[age=16,18,21]'],
                'command',
                (new ParametersBagFactory())->withOptions(['name' => 'value', 'age' => [16, 18, 21]])->create(),
                false
            ],
            [
                ['app.php', 'command', '{arg1,arg2}', '[age=16,18]', '{arg3}', '[name=value]'],
                'command',
                (new ParametersBagFactory())->withArguments(['arg1', 'arg2', 'arg3'])->withOptions(['name' => 'value', 'age' => [16, 18]])->create(),
                false
            ],
        ];
    }
}
