<?php

namespace Spiritvl\Console\Tests\Input\Parameters;

use Spiritvl\Console\Input\Parameters\Arguments;
use PHPUnit\Framework\TestCase;

class ArgumentsTest extends TestCase
{
    public function testEmpty(): void
    {
        $bag = new Arguments();

        $this->assertEmpty($bag->all());
    }

    public function testAddMany(): void
    {
        $bag = new Arguments();

        $bag->add(['some', 'arg']);

        $this->assertTrue($bag->isExist('some'));
        $this->assertTrue($bag->isExist('arg'));
        $this->assertEquals(['some', 'arg'], $bag->all());
    }
}
