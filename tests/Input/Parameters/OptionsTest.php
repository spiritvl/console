<?php

namespace Spiritvl\Console\Tests\Input\Parameters;

use Spiritvl\Console\Input\Parameters\Options;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class OptionsTest extends TestCase
{
    public function testEmpty(): void
    {
        $bag = new Options();

        $this->assertEmpty($bag->all());
    }

    public function testAddSimple(): void
    {
        $bag = new Options();

        $bag->add(['name' => 'value', 'title' => 'description']);

        $this->assertTrue($bag->isExist('name'));
        $this->assertEquals('value', $bag->get('name'));
        $this->assertTrue($bag->isExist('title'));
        $this->assertEquals('description', $bag->get('title'));
        $this->assertEquals(['name' => 'value', 'title' => 'description'], $bag->all());
    }

    public function testAddMultiple(): void
    {
        $bag = new Options();

        $bag->add(['name' => ['some', 'value']]);

        $this->assertTrue($bag->isExist('name'));
        $this->assertEquals(['some', 'value'], $bag->get('name'));
        $this->assertEquals(['name' => ['some', 'value']], $bag->all());
    }

    public function testErrorGet(): void
    {
        $bag = new Options();

        $this->expectException(RuntimeException::class);

        $bag->get('name');
    }
}
