<?php

namespace Spiritvl\Console\Tests\Input\Parsers;

use Spiritvl\Console\Input\Parsers\ArgumentsParser;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class ArgumentsParserTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testParse(string $input, string|array $output, bool $isArgument): void
    {
        $parser = new ArgumentsParser();

        $result = $parser->parse($input);

        $this->assertEquals($output, $result);
        $this->assertEquals($isArgument, $parser->isArgument($input));
    }

    public function testErrorParse(): void
    {
        $parser = new ArgumentsParser();

        $this->assertFalse($parser->isArgument('[no-arg]'));

        $this->expectException(RuntimeException::class);
        $parser->parse('[no-arg]');
    }

    public function dataProvider(): array
    {
        return [
            [
                '{arg}',
                ['arg'], true,
            ],
            [
                '{arg,arg2}',
                ['arg', 'arg2'], true,
            ],
        ];
    }
}
