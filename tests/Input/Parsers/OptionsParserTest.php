<?php

namespace Spiritvl\Console\Test\Input\Parsers;

use Spiritvl\Console\Input\Parsers\OptionsParser;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class OptionsParserTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testParse(string $input, string|array $output, bool $isArgument): void
    {
        $parser = new OptionsParser();

        $result = $parser->parse($input);

        $this->assertEquals($output, $result);
        $this->assertEquals($isArgument, $parser->isOption($input));
    }

    public function testErrorParse(): void
    {
        $parser = new OptionsParser();

        $this->assertFalse($parser->isOption('{no-opt}'));

        $this->expectException(RuntimeException::class);
        $parser->parse('{no-opt');
    }

    public function dataProvider(): array
    {
        return [
            [
                '[name=value]',
                ['name' => 'value'], true,
            ],
            [
                '[name=value,another]',
                ['name' => ['value', 'another']], true,
            ],
        ];
    }
}
