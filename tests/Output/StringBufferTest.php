<?php

namespace Spiritvl\Console\Tests\Output;

use PHPUnit\Framework\TestCase;
use Spiritvl\Console\Output\StringBuffer;

class StringBufferTest extends TestCase
{
    public function testEmptyCreate(): void
    {
        $buffer = new StringBuffer();

        $this->assertEmpty($buffer->getLines());
        $this->assertEmpty($buffer->toString());
    }

    public function testCreate(): void
    {
        $buffer = new StringBuffer(['first', 'second']);

        $this->assertEquals(['first', 'second'], $buffer->getLines());
        $this->assertEquals('first' . PHP_EOL . 'second' . PHP_EOL, $buffer->toString());
    }

    public function testAddLine(): void
    {
        $buffer = new StringBuffer();

        $buffer->addLine('first')->addLine('second');

        $this->assertEquals(['first', 'second'], $buffer->getLines());
        $this->assertEquals('first' . PHP_EOL . 'second' . PHP_EOL, $buffer->toString());
    }

    public function testAddLines(): void
    {
        $buffer = new StringBuffer();

        $buffer->addLines(['first', 'second']);

        $this->assertEquals(['first', 'second'], $buffer->getLines());
        $this->assertEquals('first' . PHP_EOL . 'second' . PHP_EOL, $buffer->toString());
    }

    public function testClear(): void
    {
        $buffer = new StringBuffer(['first', 'second']);

        $this->assertEquals(['first', 'second'], $buffer->getLines());
        $this->assertEquals('first' . PHP_EOL . 'second' . PHP_EOL, $buffer->toString());

        $buffer->clear();

        $this->assertEmpty($buffer->getLines());
        $this->assertEmpty($buffer->toString());
    }
}
